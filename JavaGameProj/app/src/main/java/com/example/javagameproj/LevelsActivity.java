package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for selecting the desired difficulty level
 * @version 1.0
 */
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.WindowManager;

public class LevelsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets the window to fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_levels); // loads the activity for selecting difficulty level



        findViewById(R.id.mMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LevelsActivity.this, MainActivity.class)); // starts main activity
            }

        });

        /**
         * loads the easy difficulty level onClick
         */
        findViewById(R.id.easyDifficulty).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LevelsActivity.this, GameActivity.class)); // loads the GameActivity class which contains the ships behaviour etc for the easy difficulty levle.
            }

        });

        /**
         * loads the medium difficulty level onClick
         */
        findViewById(R.id.mediumDifficulty).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LevelsActivity.this, GameActivity2.class)); // loads the GameActivity2 class which contains the ships behaviour etc for the medium difficulty level.
            }

        });

        /**
         * loads the hard difficulty level onClick
         */
        findViewById(R.id.hardDifficulty).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LevelsActivity.this, GameActivity3.class)); // loads the GameActivity3 class which contains the ships behaviour etc for the hard difficulty level.
            }
        });
    }
}