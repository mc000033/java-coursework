package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for setting up the hard difficulty level
 * @version 1.0
 */

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.javagameproj.GameView3;

public class GameActivity3 extends AppCompatActivity {

    private GameView3 gV3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set window to fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);

        gV3 = new GameView3(this, point.x, point.y);

        setContentView(gV3);
    }

    /**
     * method which handles the behaviour of the game when paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        gV3.pauseGame();
    }

    /**
     * method which handles the behaviour of the game when resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        gV3.resumeGame();
    }
}