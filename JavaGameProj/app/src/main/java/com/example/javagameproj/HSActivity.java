package com.example.javagameproj;
/**
 *@author Brandon Swann
 *This is a class to create an online highscore list and pass down all the functions needed
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class HSActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_highscore); // sets the correct layout for this class

        String TAG = "HighscoreActivity";
        // Toast to test the connection to firebase
        Toast.makeText(HSActivity.this, "Connected Successfully!", Toast.LENGTH_LONG).show();

        //retrieve TextView object as a variable which is used for displaying text on the screen
        TextView txtShow = findViewById(R.id.textShow);
        TextView textScore = findViewById(R.id.txtScore);

        Intent intent = getIntent();
        intent.getExtras().getInt("HighScore");
        SharedPreferences gamePrefs = getSharedPreferences("game", MODE_PRIVATE);
        int hScore = intent.getExtras().getInt("HighScore");
        textScore.setText(String.valueOf(hScore));

        // read student ID and player name object (EditText) from activity (GUI)
        EditText txtStudentID, txtPlayerName;
        txtStudentID = findViewById(R.id.txtStudID);
        txtPlayerName = findViewById(R.id.txtName);

        // retrieve read and write buttons
        Button btnWrite, btnRead;
        btnWrite = findViewById(R.id.btnWrite);
        btnRead = findViewById(R.id.btnRead);

        // write data to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Android Score");


        /**
         * method which allows you to enter and save details
         */
        int finalHScore = hScore;
        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String studID, name, score;

                studID = txtStudentID.getText().toString();
                name = txtPlayerName.getText().toString();
                score = String.valueOf(finalHScore);

                // sets the value to an object
                DataObj obj =  new DataObj(name,score);

                if(studID.equalsIgnoreCase("")) {
                    txtShow.setText("Status : Enter Value");// display name on screen
                }else{
                    // writes values to the database
                    myRef.child(studID).setValue(obj);
                    txtShow.setText("Status : Data Stored");// display name on screen
                    Toast.makeText(HSActivity.this, "Data Stored OK!", Toast.LENGTH_LONG).show();
                }
            }
        });

        /**
         *function to allow you to read the player data from the database
         */
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Read from the database
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        /* this method is called once with the initial value and again
                         whenever data at this location is updated.*/

                        ArrayList value = new ArrayList();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String  data =  ds.getKey() + ": ";
                            for (DataSnapshot dsChildren : ds.getChildren()) {
                                data = data + " " + dsChildren.getValue();
                            }
                            value.add(data); // storing into ArrayList
                        }
                        int i = 0;
                        Iterator iter = value.iterator(); // iterator for array list
                        String txtPrint = ""; // collect 5 String in five lines
                        while (iter.hasNext() && i < 5) {// iterate for five names in database
                            txtPrint = txtPrint + iter.next() + "\n"; // "\n" will enable line
                            i++; // increasing count
                        }

                        Log.d(TAG, "Value is: " + txtPrint);
                        txtShow.setText("Status : Data Retrieved" + "\n\n" + txtPrint);// display name on screen

                        txtStudentID.setText("");
                        txtPlayerName.setText("");
                    }


                    @Override
                    public void onCancelled(DatabaseError error) {
                        // failed to read value
                        Log.w(TAG, "Failed to read value.", error.toException());
                    }
                });
            }
        });
        /**
         * click on the main menu text to start the main menu
         */
        findViewById(R.id.main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //starts new activity which is the main activity
                startActivity(new Intent(HSActivity.this, MainActivity.class));
            }

        });



    }
}
