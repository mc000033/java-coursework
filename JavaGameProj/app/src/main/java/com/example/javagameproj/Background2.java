package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles the background for the medium difficulty level
 * @version 1.0
 */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.javagameproj.R;

public class Background2 {
   public int x = 0, y = 0;
   public Bitmap bg2;

    /**
     * constructor to handle background sizes
     * @param sX size of bg on x axis
     * @param sY size of bg on y axis
     * @param res resource image
     */
    public Background2 (int sX, int sY, Resources res){

        bg2 = BitmapFactory.decodeResource(res, R.drawable.background2); // draws background image from file background1
        bg2 = Bitmap.createScaledBitmap(bg2, sX, sY, false); // scales image to fit screen
    }
}
