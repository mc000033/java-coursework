package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles enemies for the hard difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView.sRatioX;
import static com.example.javagameproj.GameView.sRatioY;
import static com.example.javagameproj.GameView2.sRatioX2;
import static com.example.javagameproj.GameView2.sRatioY2;
import static com.example.javagameproj.GameView3.sRatioX3;
import static com.example.javagameproj.GameView3.sRatioY3;


public class Enemy3 {

    public int enemySpeed = 20;
    public int x;
    public int y;
    public int width;
    int height;
    int enemyCounter = 1;
    public boolean shot = true;
    Bitmap enemy3;

    public Enemy3(Resources res) {

        enemy3 = BitmapFactory.decodeResource(res, R.drawable.enemy3);

        width = enemy3.getWidth();
        height = enemy3.getHeight();

        // resize image width and height
        width /= 10;
        height /= 10;

        // make the enemies compatible with other screen sizes
        width = (int) (width * sRatioX3);
        height = (int) (height * sRatioY3);

        // scale bitmap image
        enemy3 = Bitmap.createScaledBitmap(enemy3, width, height, false);

        y =- height; // places enemy off screen at the start
    }

    /**
     * method which handles the enemy3 bitmap
     * @return enemy3 bitmap
     */
    public Bitmap getEnemy() {

        if (enemyCounter == 1) {
            enemyCounter++;
            return enemy3;
        }
        enemyCounter = 1;
        return enemy3;
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
