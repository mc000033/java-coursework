package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles the background for the hard difficulty level
 * @version 1.0
 */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.javagameproj.R;

public class Background3 {
    public int x = 0, y = 0;
    public Bitmap bg3;

    /**
     * constructor to handle background sizes
     * @param sX size of bg on x axis
     * @param sY size of bg on y axis
     * @param res resource image
     */
    public Background3 (int sX, int sY, Resources res){

        bg3 = BitmapFactory.decodeResource(res, R.drawable.background3); // draws background image from file background1
        bg3 = Bitmap.createScaledBitmap(bg3, sX, sY, false); // scales image to fit screen
    }
}
