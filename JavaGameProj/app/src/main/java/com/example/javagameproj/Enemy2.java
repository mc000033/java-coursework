package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles enemies for the medium difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView.sRatioX;
import static com.example.javagameproj.GameView.sRatioY;
import static com.example.javagameproj.GameView2.sRatioX2;
import static com.example.javagameproj.GameView2.sRatioY2;


public class Enemy2 {

    public int enemySpeed = 20;
    public int x;
    public int y;
    public int width;
    int height;
    int enemyCounter = 1;
    public boolean shot = true;
    Bitmap enemy2;

    public Enemy2(Resources res) {

        enemy2 = BitmapFactory.decodeResource(res, R.drawable.enemy2);

        width = enemy2.getWidth();
        height = enemy2.getHeight();

        // resize image width and height
        width /= 10;
        height /= 10;

        // make the enemies compatible with other screen sizes
        width = (int) (width * sRatioX2);
        height = (int) (height * sRatioY2);

        // scale bitmap image
        enemy2 = Bitmap.createScaledBitmap(enemy2, width, height, false);

        y =- height; // places enemy off screen at the start
    }

    /**
     * method which handles the enemy2 bitmap
     * @return enemy2 bitmap
     */
    public Bitmap getEnemy() {

        if (enemyCounter == 1) {
            enemyCounter++;
            return enemy2;
        }
        enemyCounter = 1;
        return enemy2;
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
