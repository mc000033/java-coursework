package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles beams for the easy difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView.sRatioX;
import static com.example.javagameproj.GameView.sRatioY;

public class Beam {

    public int x, y;
    public int width, height;
    public Bitmap beam;

    public Beam(Resources res) {

        beam = BitmapFactory.decodeResource(res, R.drawable.bullet);

        // resize beam bitmap
        width = beam.getWidth();
        height = beam.getHeight();

        width = (int) (width * sRatioX); // make width of beam compatible with other screen sizes
        height = (int) (height * sRatioY); // make height of beam compatible with other screen sizes

        // scale bitmap image
        beam = Bitmap.createScaledBitmap(beam, width, height, false);
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
