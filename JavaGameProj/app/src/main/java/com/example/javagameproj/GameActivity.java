package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for setting up the easy difficulty level
 * @version 1.0
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.javagameproj.GameView;

public class GameActivity extends AppCompatActivity {

    private GameView gV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set window to fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);

        gV = new GameView(this, point.x, point.y);

        setContentView(gV);
    }

    /**
     * method which handles the behaviour of the game when paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        gV.pauseGame();
    }

    /**
     * method which handles the behaviour of the game when resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        gV.resumeGame();
    }
}