package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles enemies for the easy difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView.sRatioX;
import static com.example.javagameproj.GameView.sRatioY;


public class Enemy1 {

    public int enemySpeed = 20;
    public int x;
    public int y;
    public int width;
    int height;
    int enemyCounter = 1;
    public boolean shot = true;
    Bitmap enemy1;

    public Enemy1(Resources res) {

        enemy1 = BitmapFactory.decodeResource(res, R.drawable.enemy1);

        // resize enemy1 bitmap
        width = enemy1.getWidth();
        height = enemy1.getHeight();

        // resize image width and height
        width /= 10;
        height /= 10;

        // make the enemies compatible with other screen sizes
        width = (int) (width * sRatioX);
        height = (int) (height * sRatioY);

        // scale bitmap image
        enemy1 = Bitmap.createScaledBitmap(enemy1, width, height, false);

        y =- height; // places enemy off screen at the start
    }


    /**
     * method which handles the enemy1 bitmap
     * @return enemy1 bitmap
     */
    public Bitmap getEnemy() {

        if (enemyCounter == 1) {
            enemyCounter++;
            return enemy1;
        }
        enemyCounter = 1;
        return enemy1;
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
