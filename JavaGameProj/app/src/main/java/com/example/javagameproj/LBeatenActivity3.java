package com.example.javagameproj;

/** @author Brandon Swann
 *
 * This class is used for progressing onto the next difficulty level or starting again.
 * @version 1.0
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class LBeatenActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets window to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_beaten3);//links the correct layout file to the class

        /**
         * click on the main menu text to start the MainActivity
         */
        findViewById(R.id.mMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LBeatenActivity3.this, MainActivity.class));
            }
        });

        /**
         * click on the start game text to start the game from the beginning
         */
        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LBeatenActivity3.this, GameActivity.class));
            }

        });

    }
}