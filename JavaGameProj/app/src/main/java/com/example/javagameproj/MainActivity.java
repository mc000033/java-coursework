package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for creating the main menu for the game.
 * @version 1.0
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

        private boolean isMuted;

    /**
     * initializes the activity
     * @param savedInstanceState loads the previously saved instance
     */
    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);


            // sets the window to fullscreen
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.activity_main); // loads the main activity which contains the layout for the main menu


            findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // start LevelsActivity
                    startActivity(new Intent(MainActivity.this, LevelsActivity.class));
                }

            });

            // assigns highscore value to textview
        TextView highScoreTxt = findViewById(R.id.hScoreTxt);

        final SharedPreferences prefs = getSharedPreferences("game", MODE_PRIVATE);
        highScoreTxt.setText("HighScore: " + prefs.getInt("highscore", 0));


            isMuted = prefs.getBoolean("isMute", false);

            //sets variable for volume image
            final ImageView volume = findViewById(R.id.volume);

            // checks if sound is muted and depending on status switch between muted and unmuted image icons.
            if (isMuted)
                volume.setImageResource(R.drawable.ic_baseline_volume_off_24);
            else
                volume.setImageResource(R.drawable.ic_baseline_volume_up_24);


            // used to handle muting and unmuting of game audio
            volume.setOnClickListener(view -> {

                isMuted = !isMuted;
                if (isMuted)
                    volume.setImageResource(R.drawable.ic_baseline_volume_off_24);
                else
                    volume.setImageResource(R.drawable.ic_baseline_volume_up_24);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isMute", isMuted);
                editor.apply();

            });



        }
    }