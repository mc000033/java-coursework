package com.example.javagameproj;

/** @author Brandon Swann
 *
 * This class is used for drawing the player on the screen for the Easy difficulty level.
 * @version 1.0
 */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.GameView3;
import com.example.javagameproj.R;

import static com.example.javagameproj.GameView2.sRatioX2;
import static com.example.javagameproj.GameView3.sRatioX3;
import static com.example.javagameproj.GameView3.sRatioY3;


public class PlayerL3 {

    public int x;
    public int y;
    public int width;
    public int height;
    public int counter = 0;
    public int toShoot = 0;
    public int shootCounter = 1;

    public boolean shipUp = false;
    Bitmap player3B, playerShoot3;
    public com.example.javagameproj.GameView3 GameView3;

    public PlayerL3(GameView3 GameView, int sY, Resources res) {
        this.GameView3 = GameView;

        player3B = BitmapFactory.decodeResource(res, R.drawable.player3);

        // resize player bitmap width and height
        width = player3B.getWidth();
        height = player3B.getHeight();


        width /= 8;
        height /= 8;

        // makes the image compatible with other screen sizes
        width = (int) (width * sRatioX3);
        height = (int) (width * sRatioY3);

        player3B = Bitmap.createScaledBitmap(player3B, width, height, false);

        playerShoot3 = BitmapFactory.decodeResource(res, R.drawable.player3s);

        playerShoot3 = Bitmap.createScaledBitmap(playerShoot3, width, height, false);

        y = sY / 2; // places image in the center of screen on y axis
        x = (int) (64 * sRatioX2);    // allows for compatibility throughout different screen sizes

    }

    /**
     * method for handling ship image
     * @return player image when player is not shooting
     */
    public Bitmap getMovement()  {

        if (toShoot != 0) {
            if (shootCounter == 1) {
                shootCounter++;
                return playerShoot3;
            }

            shootCounter = 1;
            toShoot--;

            GameView3.newBeam();

            return playerShoot3;
        }
        if (counter == 0) {
            counter++;
            return player3B;

        }
        return player3B; // return regular player image when player is not firing
    }

    /**
     * method used for creating a hitbox
     * @return rectangle positions on x and y axis
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }

}
