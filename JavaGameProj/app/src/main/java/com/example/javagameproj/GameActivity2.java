package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for setting up the medium difficulty level
 * @version 1.0
 */
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.javagameproj.GameView2;

public class GameActivity2 extends AppCompatActivity {

    private GameView2 gV2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set window to fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);

        gV2 = new GameView2(this, point.x, point.y);

        setContentView(gV2);
    }

    /**
     * method which handles the behaviour of the game when paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        gV2.pauseGame();
    }

    /**
     * method which handles the behaviour of the game when resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        gV2.resumeGame();
    }
}