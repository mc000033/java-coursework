package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for setting up the backgrounds, sound, preferences enemies and more for the easy difficulty level
 * @version 1.0
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.view.MotionEvent;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameView extends SurfaceView implements Runnable {
    private Thread thread;
    private boolean playing;
    private boolean gameLost = false;
    private Background bg1, bg2; // two background instances to allow for the background to move.
    public int sX, sY, cScore = 0, sound;
    private Paint paint;
    private List<Beam> beams;
    private Enemy1[] enemies;
    public PlayerL1 playerL1F;
    private SharedPreferences gamePrefs;
    private GameActivity activity;
    private SoundPool soundP;
    private Random random;
    public static float  sRatioX, sRatioY; // variables for handling different screen sizes
    private Intent intent;


    public GameView(GameActivity activity, int sX, int sY) {
        super(activity);
        this.activity = activity;
        // instantiates the sharedpreferences with name "game" and making the mode private which hides the content of the SharedPreferences.
        gamePrefs = activity.getSharedPreferences("game", activity.MODE_PRIVATE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .build();

            soundP = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else
            soundP = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        sound = soundP.load(activity, R.raw.shoot, 1); // assigns sound file to soundPool
        this.sX = sX;
        this.sY = sY;
        sRatioX = 1920f / sX; // sets sRatioX so its compatible with different sizes on the x axis
        sRatioY = 1080f / sY; // sets sRatioY so its compatible with different sizes on the y axis

        bg1 = new Background(sX, sY, getResources());
        bg2 = new Background(sX, sY, getResources());

        playerL1F = new PlayerL1(this, sY, getResources());

        beams = new ArrayList<>();

        bg2.x = sX;
        paint = new Paint();
        paint.setTextSize(128); // sets size of the text
        paint.setColor(Color.WHITE); // sets text color

        enemies = new Enemy1[4]; // initialises an array of 4 which means 4 enemies on the screen at any time

        for (int i = 0; i < 4; i++) {

            Enemy1 enemy = new Enemy1(getResources());
            enemies[i] = enemy;
        }

        random = new Random(); // initalises the random object which is used for randomly generating enemy speeds
    }

    /**
     * method which handles running the game, using update, draw and sleep methods while the game is playing.
     */
    @Override
    public void run() {

        while (playing){

            update(); // updates the background, beams, player and enemies as well as handling hit detection
            draw(); // draws the images to the canvas
            sleep(); // used to create an animation making it look like the game is moving
        }
    }

    /**
     * method for handling updating the background, player, enemy and beams
     */
    private void update (){

        // moves background 20px each method call
        bg1.x -= 20 * sRatioX;
        bg2.x -= 20 * sRatioX;

        if (bg1.x + bg1.bg.getWidth() < 0) {
            bg1.x = sX;
        }

        if (bg2.x + bg2.bg.getWidth() < 0) {
            bg2.x = sX;
        }

        // if shipUp is true move the player up by 30px and if its false, move player down 30px
        if (playerL1F.shipUp) {
            playerL1F.y -= 30 * sRatioY;
        } else {
            playerL1F.y += 30 * sRatioY;
        }

        // ensures player is always on the screen
        if (playerL1F.y < 0) {
            playerL1F.y = 0;
        }

        if (playerL1F.y >= sY - playerL1F.height) {
            playerL1F.y = sY - playerL1F.height;
        }

        // new ArrayList for off screen beams
        List<Beam> offScreen = new ArrayList<>();

        for (Beam beam : beams) {

            if (beam.x > sX)
                offScreen.add(beam); // add offScreen beams to the list

            beam.x += 50 * sRatioX;

            for (Enemy1 enemy1 : enemies) {
                if (Rect.intersects(enemy1.getHitBox(), beam.getHitBox())) {
                    cScore++; // increment current score by 1
                    enemy1.x = -500; // enemy is off screen
                    beam.x = sX + 500; // bullet is off screen
                    enemy1.shot = true;

                }
            }
        }
        // remove beams from offScreen list
        for (Beam beam : offScreen)
            beams.remove(beam);

        for (Enemy1 enemy1 : enemies) {
            enemy1.x -= enemy1.enemySpeed;

            /*
            checks to see if the enemy x position is less than 0
            if it is it means its off the screen which results in the gameLost condition becoming true.
             */
            if (enemy1.x + enemy1.width < 0) {

                if (!enemy1.shot) {
                    gameLost = true;
                    return;
                }
                // used to randomly generate an integer value for the enemy speeds with a maximum of 30
                int bound = (int) (30 * sRatioX);
                enemy1.enemySpeed = random.nextInt(bound);

                // if the randomly generated integer is less than 10 assign the enemySpeed to 10
                if (enemy1.enemySpeed < 10 * sRatioX)
                    enemy1.enemySpeed = (int) (10 * sRatioX);

                enemy1.x = sX;
                enemy1.y = random.nextInt(sY - playerL1F.height); // randomly generate y position of enemy

                enemy1.shot = false;
            }

            /*
            checks to see if the enemy hitbox and player hitboxes collide
            if they do, set gameLost to true
             */
            if (Rect.intersects(enemy1.getHitBox(), playerL1F.getHitBox())) {
                gameLost = true;
                return;
            }
        }
    }

    /**
     * method for handling drawing to the screen
     */
    private void draw(){

        // ensures surfaceview has been successfully initiated
        if (getHolder().getSurface().isValid()) {
            Canvas can = getHolder().lockCanvas();
            can.drawBitmap(bg1.bg, bg1.x, bg1.y, paint);
            can.drawBitmap(bg2.bg, bg2.x, bg2.y, paint);

            // draw enemy bitmap
            for (Enemy1 enemy1 : enemies) {
                can.drawBitmap(enemy1.getEnemy(), enemy1.x, enemy1.y, paint);
            }
            // draws score on the screen using concatenation through an empty string to make it a string as score is an integer
            can.drawText(cScore + "", sX / 2f, 164, paint);


            if (gameLost) {
                playing = false; // sets playing status to false#
                getHolder().unlockCanvasAndPost(can);
                exitTimer();    // set a small delay before changing activity
                this.intent = new Intent(activity, LBeatenActivity.class); // starts LBeatenActviity
                saveHScore(); // saves high score

                activity.startActivity(this.intent);
                return;
            }

            can.drawBitmap(playerL1F.getMovement(), playerL1F.x, playerL1F.y, paint); // draws player movement

            for (Beam beam : beams)
                can.drawBitmap(beam.beam, beam.x, beam.y, paint); // draws beams
            getHolder().unlockCanvasAndPost(can);
        }
    }

    /**
     * method for setting an exit delay
     */
    private void exitTimer() {
        try {
            Thread.sleep(3000); // sets a sleep timer of 3 seconds
            activity.startActivity(new Intent(activity, LevelsActivity.class)); // starts activity
            activity.finish(); // finish activity
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * method for saving the new high score
     * @return return old high score
     */
    public int saveHScore() {
        if (gamePrefs.getInt("highscore", 0) < cScore) {
            SharedPreferences.Editor editor = gamePrefs.edit();
            editor.putInt("highscore", cScore);
            editor.apply();
        }

        this.intent.putExtra("HighScore", cScore);

        return cScore;
    }
    /**
     * method for setting short delays
     */
    private void sleep(){
        try{
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * method for resuming the game
     */
    public void resumeGame(){
        playing = true;
        thread = new Thread(this); // initalises thread
        thread.start();

    }

    /**
     *  method for handling how the game behaves when the game is paused
     */
    public void pauseGame(){
        try {
            playing = false; // sets playing status to false to indicate the game has been paused
            thread.join(); // terminates the thread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * method for handling player input to move the ship up and down
     * @param event handles the behaviour of the ships when the action_down and action_up events are triggered
     * @return true if event has been triggered
     */
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < sX / 6) {
                    playerL1F.shipUp = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                playerL1F.shipUp = false;
                if (event.getX() > sX / 5.5) // if player is touching the right side of the screen, shoot beam
                    playerL1F.toShoot++;
                break;
        }
        return true;
    }


    /**
     * method for handling the beam creation such as placement and the beam sound effect.
     */
    public void newBeam() {
        // if game is not muted then play sound
        if (!gamePrefs.getBoolean("isMute", false))
            soundP.play(sound, 1, 1, 0, 0, 1);

        Beam beam = new Beam(getResources());
        // places the beams near the firing point of the ship
        beam.x = playerL1F.x + playerL1F.width;
        beam.y = playerL1F.y + (playerL1F.height / 2);
        beams.add(beam);
    }


}
