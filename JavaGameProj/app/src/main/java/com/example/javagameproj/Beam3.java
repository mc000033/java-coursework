package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles beams for the hard difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView3.sRatioX3;
import static com.example.javagameproj.GameView3.sRatioY3;

public class Beam3 {

    public int x, y, width, height;
    public Bitmap beam3;

    public Beam3(Resources res) {

        beam3 = BitmapFactory.decodeResource(res, R.drawable.bullet3);

        // resize beam3 bitmap
        width = beam3.getWidth();
        height = beam3.getHeight();

        width = (int) (width * sRatioX3); // make width of beam compatible with other screen sizes
        height = (int) (height * sRatioY3); // make height of beam compatible with other screen sizes

        // scale bitmap image
        beam3 = Bitmap.createScaledBitmap(beam3, width, height, false);
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
