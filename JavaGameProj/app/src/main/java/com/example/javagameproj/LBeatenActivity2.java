package com.example.javagameproj;

/** @author Brandon Swann
 *
 * This class is used for progressing onto the next difficulty level or starting again.
 * @version 1.0
 */
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class LBeatenActivity2 extends AppCompatActivity {

    /**
     * initializes the activity
     * @param savedInstanceState loads the previously saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // sets window to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_beaten2);//links the correct layout file to the class

        /**
         * click on the main menu text to start the MainActivity
         */
        findViewById(R.id.mMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LBeatenActivity2.this, MainActivity.class));
            }
        });

        /**
         * click on the start game text to start the game from the beginning
         */
        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LBeatenActivity2.this, GameActivity.class));
            }

        });

        /**
         * click on the next difficulty text to proceeed to the next difficulty level
         */
        findViewById(R.id.nextDifficultyLevel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LBeatenActivity2.this, GameActivity3.class));
            }

        });



    }
}