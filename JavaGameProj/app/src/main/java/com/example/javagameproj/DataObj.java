package com.example.javagameproj;

/**
 * This class handles the names and scores sent to the database
 */

public class DataObj {

    String name, score;

    public DataObj(String name, String score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}


