package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles the background for the easy difficulty level
 * @version 1.0
 */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.javagameproj.R;

public class Background {

    public int x = 0, y = 0;
    public Bitmap bg;

    /**
     * constructor to handle background sizes
     * @param sX size of bg on x axis
     * @param sY size of bg on y axis
     * @param res resource image
     */
    public Background (int sX, int sY, Resources res){

        bg = BitmapFactory.decodeResource(res, R.drawable.background1); // draws background image from file background1
        bg = Bitmap.createScaledBitmap(bg, sX, sY, false); // scales image to fit screen
    }

}
