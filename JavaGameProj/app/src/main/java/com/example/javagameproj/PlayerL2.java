package com.example.javagameproj;

/** @author Brandon Swann
 *
 * This class is used for drawing the player on the screen for the Easy difficulty level.
 * @version 1.0
 */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.GameView2;
import com.example.javagameproj.R;

import static com.example.javagameproj.GameView2.sRatioX2;
import static com.example.javagameproj.GameView2.sRatioY2;


public class PlayerL2 {

    public int x;
    public int y;
    public int width;
    public int height;
    public int counter = 0;
    public int toShoot = 0;
    public int shootCounter = 1;

    public boolean shipUp = false;
    Bitmap player2B, playerShoot2;
    public com.example.javagameproj.GameView2 GameView2;

    public PlayerL2(GameView2 GameView, int sY, Resources res) {
        this.GameView2 = GameView;

        player2B = BitmapFactory.decodeResource(res, R.drawable.player2);

        // resize player bitmap width and height
        width = player2B.getWidth();
        height = player2B.getHeight();


        width /= 8;
        height /= 8;

        // makes the image compatible with other screen sizes
        width = (int) (width * sRatioX2);
        height = (int) (width * sRatioY2);

        player2B = Bitmap.createScaledBitmap(player2B, width, height, false);

        playerShoot2 = BitmapFactory.decodeResource(res, R.drawable.player2s);

        playerShoot2 = Bitmap.createScaledBitmap(playerShoot2, width, height, false);

        y = sY / 2; // places image in the center of screen on y axis
        x = (int) (64 * sRatioX2);    // allows for compatibility throughout different screen sizes

    }

    /**
     * method for handling ship image
     * @return player image when player is not shooting
     */
    public Bitmap getMovement()  {

        if (toShoot != 0) {
            if (shootCounter == 1) {
                shootCounter++;
                return playerShoot2;
            }

            shootCounter = 1;
            toShoot--;

            GameView2.newBeam();

            return playerShoot2;
        }
        if (counter == 0) {
            counter++;
            return player2B;

        }
        return player2B; // return regular player image when player is not firing
    }

    /**
     * method used for creating a hitbox
     * @return rectangle positions on x and y axis
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }

}
