package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class handles beams for the medium difficulty level
 * @version 1.0
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.R;

import static com.example.javagameproj.GameView2.sRatioX2;
import static com.example.javagameproj.GameView2.sRatioY2;

public class Beam2 {

    public int x, y, width, height;
    public Bitmap beam2;

    public Beam2(Resources res) {

        beam2 = BitmapFactory.decodeResource(res, R.drawable.bullets2);

        // resize beam2 bitmap
        width = beam2.getWidth();
        height = beam2.getHeight();

        width = (int) (width * sRatioX2); // make width of beam compatible with other screen sizes
        height = (int) (height * sRatioY2); // make height of beam compatible with other screen sizes

        // scale bitmap image
        beam2 = Bitmap.createScaledBitmap(beam2, width, height, false);
    }

    /**
     *  draws a rectangle which is used as the hitbox for both the player and enemies
     * @return new rectangle dimensions
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }
}
