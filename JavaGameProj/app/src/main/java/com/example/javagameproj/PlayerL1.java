package com.example.javagameproj;

 /** @author Brandon Swann
        *
        * This class is used for drawing the player on the screen for the Easy difficulty level.
        * @version 1.0
        */
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.javagameproj.GameView;
import com.example.javagameproj.R;

import static com.example.javagameproj.GameView.sRatioX;
import static com.example.javagameproj.GameView.sRatioY;


public class PlayerL1 {

    public int x;
    public int y;
    public int width;
    public int height;
    public int counter = 0;
    public int toShoot = 0;
    public int shootCounter = 1;

    public boolean shipUp = false;
    Bitmap player1B, playerShoot1;
    private com.example.javagameproj.GameView GameView;

    public PlayerL1(com.example.javagameproj.GameView GameView, int sY, Resources res) {
        this.GameView = GameView;

        player1B = BitmapFactory.decodeResource(res, R.drawable.player2image);

        // resize player bitmap width and height
        width = player1B.getWidth();
        height = player1B.getHeight();


        width /= 8;
        height /= 8;

        // makes the image compatible with other screen sizes
        width = (int) (width * sRatioX);
        height = (int) (height * sRatioY);

        player1B = Bitmap.createScaledBitmap(player1B, width, height, false);

        playerShoot1 = BitmapFactory.decodeResource(res, R.drawable.player1s);

        playerShoot1 = Bitmap.createScaledBitmap(playerShoot1, width, height, false);

        y = sY / 2; // places image in the center of screen on y axis
        x = (int) (64 * sRatioX);    // allows for compatibility throughout different screen sizes

    }

    /**
     * method for handling ship image
     * @return player image when player is not shooting
     */
    public Bitmap getMovement()  {

        if (toShoot != 0) {
            if (shootCounter == 1) {
                shootCounter++;
                return playerShoot1;
            }

            shootCounter = 1;
            toShoot--;

            GameView.newBeam();

            return playerShoot1;
        }
        if (counter == 0) {
            counter++;
            return player1B;

        }
        return player1B;
    }

    /**
     * method used for creating a hitbox
     * @return rectangle positions on x and y axis
     */
    public Rect getHitBox() {
        return new Rect(x, y, x + width, y + height);
    }

}
