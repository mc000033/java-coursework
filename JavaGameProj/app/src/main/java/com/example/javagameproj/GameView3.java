package com.example.javagameproj;

/**
 * @author Brandon Swann
 *
 * This class is used for setting up the backgrounds, sound, preferences enemies and more for the hard difficulty level
 * @version 1.0
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.view.MotionEvent;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameView3 extends SurfaceView implements Runnable {
    private Thread thread;
    private boolean playing;
    private boolean gameLost = false;
    private Background3 bg1, bg2; // two background instances to allow for the background to move.
    public int sX, sY, cScore = 0, sound;
    private Paint paint;
    private List<Beam3> bullets3;
    private Enemy3[] enemies3;
    public PlayerL3 playerL3F;
    private SharedPreferences gamePrefs;
    public GameActivity3 activity;
    private SoundPool soundP;
    private Random random;
    public static float sRatioX3, sRatioY3; // variables for handling different screen sizes


    public GameView3(GameActivity3 activity, int sX, int sY) {
        super(activity);
        this.activity = activity;
        // instantiates the sharedpreferences with name "game" and making the mode private which hides the content of the SharedPreferences.
        gamePrefs = activity.getSharedPreferences("game", Context.MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .build();

            soundP = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else
            soundP = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        sound = soundP.load(activity, R.raw.shoot, 1);
        this.sX = sX;
        this.sY = sY;
        sRatioX3 = 1920f / sX; // sets sRatioX3 so its compatible with different sizes on the x axis
        sRatioY3 = 1080f / sY; // sets sRatioY3 so its compatible with different sizes on the y axis

        bg1 = new Background3(sX, sY, getResources());
        bg2 = new Background3(sX, sY, getResources());

        playerL3F = new PlayerL3(this, sY, getResources());

        bullets3 = new ArrayList<>();

        bg2.x = sX;
        paint = new Paint();
        paint.setTextSize(128); // sets size of the text
        paint.setColor(Color.WHITE); // sets text color

        enemies3 = new Enemy3[9]; // initialises an array of 8 which means 8 enemies on the screen at any time

        for (int i = 0; i < 9; i++) {

            Enemy3 enemy = new Enemy3(getResources());
            enemies3[i] = enemy;
        }

        random = new Random(); // initalises the random object which is used for randomly generating enemy speeds
    }

    /**
     * method which handles running the game, using update, draw and sleep methods while the game is playing.
     */
    @Override
    public void run() {

        while (playing){

            update(); // updates the background, beams, player and enemies as well as handling hit detection
            draw(); // draws the images to the canvas
            sleep(); // used to create an animation making it look like the game is moving
        }
    }

    /**
     * method for handling updating the background, player, enemy and beams
     */
    private void update (){

        // moves background 20px each method call
        bg1.x -= 20 * sRatioX3;
        bg2.x -= 20 * sRatioX3;

        if (bg1.x + bg1.bg3.getWidth() < 0) {
            bg1.x = sX;
        }

        if (bg2.x + bg2.bg3.getWidth() < 0) {
            bg2.x = sX;
        }

        // if shipUp is true move the player up by 30px and if its false, move player down 30px
        if (playerL3F.shipUp) {
            playerL3F.y -= 30 * sRatioY3;
        } else {
            playerL3F.y += 30 * sRatioY3;
        }

        // ensures player is always on the screen
        if (playerL3F.y < 0) {
            playerL3F.y = 0;
        }

        if (playerL3F.y >= sY - playerL3F.height) {
            playerL3F.y = sY - playerL3F.height;
        }

        // new ArrayList for off screen beams
        ArrayList<Beam3> trash = new ArrayList<>();

        for (Beam3 beam3 : bullets3) {

            if (beam3.x > sX)
                trash.add(beam3); // add offScreen beams to the list

            beam3.x += 50 * sRatioX3;

            for (Enemy3 enemy3 : enemies3) {
                if (Rect.intersects(enemy3.getHitBox(), beam3.getHitBox())) {
                    cScore++; // increment current score by 1
                    enemy3.x = -500; // enemy is off screen
                    beam3.x = sX + 500; // bullet is off screen
                    enemy3.shot = true;
                }
            }
        }

        // remove beams from offScreen list
        for (Beam3 beam3 : trash)
            bullets3.remove(beam3);

        for (Enemy3 enemy3 : enemies3) {
            enemy3.x -= enemy3.enemySpeed;

            /*
            checks to see if the enemy x position is less than 0
            if it is it means its off the screen which results in the gameLost condition becoming true.
             */
            if (enemy3.x + enemy3.width < 0) {

                if (!enemy3.shot) {
                    gameLost = true;
                    return;
                }

                // used to randomly generate an integer value for the enemy speeds with a maximum of 55
                int bound = (int) (55 * sRatioX3);
                enemy3.enemySpeed = random.nextInt(bound);

                // if the randomly generated integer is less than 20 assign the enemySpeed to 20
                if (enemy3.enemySpeed < 20 * sRatioX3)
                    enemy3.enemySpeed = (int) (20 * sRatioX3);

                enemy3.x = sX;
                enemy3.y = random.nextInt(sY - playerL3F.height); // randomly generate y position of enemy

                enemy3.shot = false;
            }

            /*
            checks to see if the enemy hitbox and player hitboxes collide
            if they do, set gameLost to true
             */
            if (Rect.intersects(enemy3.getHitBox(), playerL3F.getHitBox())) {
                gameLost = true;
                return;
            }

        }
    }

    /**
     * method for handling drawing to the screen
     */
    private void draw(){

        // ensures surfaceview has been successfully initiated
        if (getHolder().getSurface().isValid()) {
            Canvas can = getHolder().lockCanvas();
            can.drawBitmap(bg1.bg3, bg1.x, bg1.y, paint);
            can.drawBitmap(bg2.bg3, bg2.x, bg2.y, paint);

            // draw enemy bitmap
            for (Enemy3 enemy3 : enemies3) {
                can.drawBitmap(enemy3.getEnemy(), enemy3.x, enemy3.y, paint);
            }
            // draws score on the screen using concatenation through an empty string to make it a string as score is an integer
            can.drawText(cScore + "", sX / 2f, 164, paint);

            if (gameLost) {
                playing = false; // sets playing status to false
                saveHScore(); // saves high score
                exitTimer(); // set a small delay before changing activity
                activity.startActivity(new Intent(activity, LBeatenActivity3.class));
                getHolder().unlockCanvasAndPost(can);
                return;
            }

            can.drawBitmap(playerL3F.getMovement(), playerL3F.x, playerL3F.y, paint); // draws player movement

            for (Beam3 beam3 : bullets3)
                can.drawBitmap(beam3.beam3, beam3.x, beam3.y, paint); // draws beams
            getHolder().unlockCanvasAndPost(can);
        }
    }

    /**
     * method for setting an exit delay
     */
    private void exitTimer() {
        try {
            Thread.sleep(3000);  // sets a sleep timer of 3 seconds
            activity.startActivity(new Intent(activity, LevelsActivity.class)); // starts activity
            activity.finish(); // finish activity
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * method for saving the new high score
     * @return return old high score
     */
    private int saveHScore() {
        if (gamePrefs.getInt("highscore", 0)  < cScore) {
            SharedPreferences.Editor editor = gamePrefs.edit();
            editor.putInt("highscore", cScore);
            editor.apply();
        }
        return cScore;
    }

    /**
     * method for setting short delays
     */
    private void sleep(){
        try{
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * method for resuming the game
     */
    public void resumeGame(){
        playing = true;
        thread = new Thread(this); // initalises thread
        thread.start();

    }

    /**
     *  method for handling how the game behaves when the game is paused
     */
    public void pauseGame(){
        try {
            playing = false; // sets playing status to false to indicate the game has been paused
            thread.join(); // terminates the thread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * method for handling player input to move the ship up and down
     * @param event
     * @return true if event has been triggered
     */
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < sX / 6) {
                    playerL3F.shipUp = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                playerL3F.shipUp = false;
                if (event.getX() > sX / 5.5) // if player is touching the right side of the screen, shoot beam
                    playerL3F.toShoot++;
                break;
        }
        return true;
    }

    /**
     * method for handling the beam creation such as placement and the beam sound effect.
     */
    public void newBeam() {
        // if game is not muted then play sound
        if (!gamePrefs.getBoolean("isMute", false))
            soundP.play(sound, 1, 1, 0, 0, 1);

        Beam3 beam3 = new Beam3(getResources());
        // places the beams near the firing point of the ship
        beam3.x = playerL3F.x + playerL3F.width;
        beam3.y = playerL3F.y + (playerL3F.height / 2);
        bullets3.add(beam3);
    }

}
